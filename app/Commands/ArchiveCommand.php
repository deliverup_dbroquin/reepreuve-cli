<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ArchiveCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:archive';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Make archive from past year campaigns';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        // Format date to knk format -> 'YYYYMMDD'
        //$knkFormated = now()->year.'1232';

        // For tests
        $now = Carbon::parse('2014-3-19')->year;
        $knkFormated = $now.'1232';

        /** ---------------------------------------------------------
            * Fetch rows
        */
        $campaigns = DB::table('epr_campagne')->where('camp_date', '<' , $knkFormated)->get();
        $this->info('Campaigns successfully fetched (' . $campaigns->count() . ' records)');

        /** ---------------------------------------------------------
            * Store rows in archive db
        */
        $toStore = [];

        // Build row for insert
        foreach($campaigns as $campaign) {
            $row = [];
            foreach(getCampaignColumns() as $col) {
                $row[$col] = $campaign->{$col};
            }

            array_push($toStore, $row);
        }
        $this->info('Records has been reformated for insertion');

        DB::connection('archives')->table('epr_campagne')->insert($toStore);
        $this->info('Records has been copied to archive database');

        /** ---------------------------------------------------------
         * Purge production db
        */
        DB::table('epr_campagne')->where('camp_date', '<' , $knkFormated)->delete();

        // Done !
        $this->info('Records has been deleted from source database');
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
        // Do all years
		$schedule->command(static::class)->cron('0 0 1 1 *');
	}
}
