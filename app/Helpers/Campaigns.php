<?php

use Illuminate\Support\Facades\DB;

/**
 * Fetch columns names from "epr_campagne" table
 *
 * @return Array
 */
function getCampaignColumns() {
    return DB::connection('production')->getSchemaBuilder()->getColumnListing('epr_campagne');
}