<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */
    'connections' => [
        'default' => [
            'driver' => 'mysql',
            'host' => env('RP_HOST', '127.0.0.1'),
            'port' => env('RP_PORT', '3306'),
            'database' => env('RP_DATABASE', 'forge'),
            'username' => env('RP_USERNAME', 'forge'),
            'password' => env('RP_PASSWORD', ''),
            'unix_socket' => env('RP_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'archives' => [
            'driver' => 'mysql',
            'host' => env('AR_HOST', '127.0.0.1'),
            'port' => env('AR_PORT', '3306'),
            'database' => env('AR_DATABASE', 'forge'),
            'username' => env('AR_USERNAME', 'forge'),
            'password' => env('AR_PASSWORD', ''),
            'unix_socket' => env('AR_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
    ],

];
