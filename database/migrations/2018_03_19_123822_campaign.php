<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Campaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('archives')->create('epr_campagne' , function (Blueprint $table) {
            foreach(getCampaignColumns() as $col) {
                if($col === 'camp_codeprop' ||
                    $col === 'camp_numbouteille' ||
                    $col === 'camp_statut' ||
                    $col === 'camp_illisible' ||
                    $col === 'camp_sync'
                ) {
                    $table->integer($col);
                } elseif($col === 'camp_typemotifref') {
                    $table->char($col, 1);
                } else {
                    $table->string($col);
                }
            }
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
